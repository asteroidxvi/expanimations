package com.example.android.animationsdemo;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

/**
 * Created by ag on 7/30/13.
 */
public class CoolDialogFragment extends DialogFragment {


    /* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    public interface CoolDialogListener {
        public void onDialogIncrementClick(DialogFragment dialog);
        public void onDialogQuitClick(DialogFragment dialog);
    }

    // Use this instance of the interface to deliver action events
    CoolDialogListener mListener;

    // Override the Fragment.onAttach() method
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {

            mListener = (CoolDialogListener) activity;
        } catch (ClassCastException e) {

            throw new ClassCastException(activity.toString()
                    + " must implement CoolDialogFragment");
        }
    }

    //this will be used when inflating, like onCreate, or onCreateView or fragment
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Build the dialog and set up the button click handlers
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_signin, null);
        builder.setView(v);
        builder

                .setPositiveButton("Increment", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Send the positive button event back to the host activity
                        mListener.onDialogIncrementClick(CoolDialogFragment.this);


                    }
                })
                .setNegativeButton("Quit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                            mListener.onDialogQuitClick(CoolDialogFragment.this);

                    }
                });
        return builder.create();
    }

}
